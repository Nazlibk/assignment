package ie.nazli.assignment;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DataFactoryTest {

    Map<Integer, Student> data = new LinkedHashMap<>();

    @Before
    public void GenerateInitialData() {
        DataFactory df = new DataFactory();
        Student student;
        Date minBirthDate = df.getDate(1989, 1, 1);
        Date maxBirthDate = df.getDate(1999, 1, 1);
        int n = 10;

        for (int i = 0; i < n; i++) {
            student = new Student.Builder().withId(i + 1)
                                           .withFirstName(df.getFirstName())
                                           .withSurname(df.getLastName())
                                           .withAddress(df.getAddress())
                                           .withBirthDate(df.getDateBetween(minBirthDate, maxBirthDate))
                                           .withBirthCity(df.getCity())
                                           .build();
            data.put(i + 1, student);
        }
    }

    @Test
    public void test_write_on_csv_file() throws Exception {
        Utiities utiities = new Utiities();
        utiities.writeOnCsvFile(data, "data");
    }

    @Test
    public void test_read_from_csv_file() throws Exception {
        Utiities utiities = new Utiities();
        data = utiities.readFromCsvFile("data");
        for (int i = 0; i < data.size(); i++) {
            System.out.println(data.get(i + 1).toString());
        }
    }

    @Test
    public void test_write_on_xml_file() throws Exception {
        Utiities utiities = new Utiities();
        utiities.writeOnXmlFile(data, "data");
    }

    @Test
    public void test_read_from_xml_file() throws Exception {
        Utiities utiities = new Utiities();
        data = utiities.ReadFromXmlFile("data");
        for (int i = 0; i < data.size(); i++) {
            System.out.println(data.get(i + 1).toString());
        }
    }

    @Test
    public void test_add_new_student() {
        DataFactory df = new DataFactory();
        Student student = new Student.Builder().withId(data.size() + 1).withFirstName("Nazli")
                                               .withSurname("Bagherzadeh")
                                               .withAddress("38 Newcastle Road")
                                               .withBirthDate(df.getDate(1989, 02, 22))
                                               .withBirthCity("Tabriz").build();
        int datasFirstSize = data.size();
        data = student.addNewStudent(student, data);
        assertThat(datasFirstSize + 1, is(data.size()));
    }

    @Test
    public void test_add_exsiting_student() {
        DataFactory df = new DataFactory();
        Student student = new Student.Builder().withId(data.size() + 1).withFirstName("Nazli")
                                               .withSurname("Bagherzadeh")
                                               .withAddress("38 Newcastle Road")
                                               .withBirthDate(df.getDate(1989, 02, 22))
                                               .withBirthCity("Tabriz").build();
        data = student.addNewStudent(student, data);
        int datasFirstSize = data.size();
        data = student.addNewStudent(student, data);
        assertThat(datasFirstSize, is(data.size()));
    }

    @Test
    public void test_delete_existent_student() {
        DataFactory df = new DataFactory();
        Student student = new Student.Builder().withId(data.size() + 1)
                                               .withFirstName("Nazli")
                                               .withSurname("Bagherzadeh")
                                               .withAddress("38 Newcastle Road")
                                               .withBirthDate(df.getDate(1989, 02, 22))
                                               .withBirthCity("Tabriz").build();
        data = student.addNewStudent(student, data);
        int datasFirstSize = data.size();
        data = student.deleteStudent(student.getFullName(), data);
        assertThat(datasFirstSize - 1, is(data.size()));
    }

    @Test
    public void test_delete_nonexistent_student() {
        DataFactory df = new DataFactory();
        Student student = new Student.Builder().withId(data.size() + 1)
                                               .withFirstName("aaa")
                                               .withSurname("bbb")
                                               .withAddress("38 Newcastle Road")
                                               .withBirthDate(df.getDate(1989, 02, 22))
                                               .withBirthCity("Tabriz").build();
        int datasFirstSize = data.size();
        data = student.deleteStudent(student.getFullName(), data);
        assertThat(datasFirstSize, is(data.size()));
    }

    @Test
    public void test_update_existent_student() {
        DataFactory df = new DataFactory();
        Student oldStudent = new Student.Builder().withId(data.size() + 1)
                                                  .withFirstName("Nazliil")
                                                  .withSurname("Bagherzadeh")
                                                  .withAddress("38 Newcastle Road")
                                                  .withBirthDate(df.getDate(1989, 02, 22))
                                                  .withBirthCity("Tabriz").build();
        Student newStudent = new Student.Builder().withId(data.size() + 1)
                                                  .withFirstName("Nazli")
                                                  .withSurname("Bagherzadeh")
                                                  .withAddress("38 Newcastle Road")
                                                  .withBirthDate(df.getDate(1989, 02, 22))
                                                  .withBirthCity("Tabriz")
                                                  .build();
        data = oldStudent.addNewStudent(oldStudent, data);
        int datasFirstSize = data.size();
        data = newStudent.updateStudent(oldStudent.getFullName(), newStudent, data);
        assertThat(datasFirstSize, is(data.size()));
    }

    @Test
    public void test_update_nonexistent_student() {
        DataFactory df = new DataFactory();
        Student student = new Student.Builder().withId(data.size() + 1)
                                               .withFirstName("Nazli")
                                               .withSurname("Bagherzadeh")
                                               .withAddress("38 Newcastle Road")
                                               .withBirthDate(df.getDate(1989, 02, 22))
                                               .withBirthCity("Tabriz")
                                               .build();
        int datasFirstSize = data.size();
        data = student.updateStudent(student.getFullName(), student, data);
        assertThat(datasFirstSize, is(data.size()));
    }

    @After
    public void tearDown() {

    }

}
