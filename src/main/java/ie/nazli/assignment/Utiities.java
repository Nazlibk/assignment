package ie.nazli.assignment;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class Utiities {

    public Map<Integer, Student> readFromCsvFile(String fileName) throws AssignmentExceptions {

        final String[] FILE_HEADER = {"ID", "First name", "Surname", "Address", "Birth Date", "Birth city"};
        Map<Integer, Student> data = new LinkedHashMap<>();
        FileReader fileReader = null;
        CSVParser csvFileParser = null;
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER);
        try {
            fileReader = new FileReader(fileName + ".csv");
            csvFileParser = new CSVParser(fileReader, csvFileFormat);
            List<CSVRecord> csvRecords = csvFileParser.getRecords();
            for (int i = 1; i < csvRecords.size(); i++) {
                CSVRecord record = csvRecords.get(i);
                Student student = new Student.Builder()
                        .withId(Integer.parseInt(record.get("ID")))
                        .withFirstName(record.get("First name"))
                        .withSurname(record.get("Surname"))
                        .withAddress(record.get("Address"))
                        .withBirthDate(new SimpleDateFormat("E MMM dd HH:mm:ss zzz yyyy").parse(record.get("Birth Date")))
                        .withBirthCity(record.get("Birth city")).build();
                data.put(student.getId(), student);
            }
        } catch (Exception e) {
            throw new AssignmentExceptions("Couldn't read from file " + fileName + ".", e);
        } finally {
            try {
                fileReader.close();
                csvFileParser.close();
            } catch (IOException e) {
                throw new AssignmentExceptions("Error while closing fileReader/csvFileParser !!!", e);
            }
        }
        return data;
    }

    public void writeOnCsvFile(Map<Integer, Student> data, String fileName) throws AssignmentExceptions {

        final Object[] FILE_HEADER = {"ID", "First name", "Surname", "Address", "Birth Date", "Birth city"};
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
        try {

            fileWriter = new FileWriter(fileName + ".csv");
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
            csvFilePrinter.printRecord(FILE_HEADER);
            for (Student student : data.values()) {
                List<String> studentDataRecord = new ArrayList();
                studentDataRecord.add(String.valueOf(student.getId()));
                studentDataRecord.add(student.getFirstName());
                studentDataRecord.add(student.getSurname());
                studentDataRecord.add(student.getAddress());
                studentDataRecord.add(String.valueOf(student.getBirthDate()));
                studentDataRecord.add(student.getBirthCity());
                csvFilePrinter.printRecord(studentDataRecord);
            }
        } catch (Exception e) {
            throw new AssignmentExceptions("Couldn't open " + fileName + ".", e);
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                throw new AssignmentExceptions("Error while flushing/closing fileWriter/csvPrinter!!!", e);
            }
        }
    }

    public void writeOnXmlFile(Map<Integer, Student> data, String fileName) throws AssignmentExceptions {
        try {
            Students students = new Students();
            List<Student> studentsList = new ArrayList<>(data.values());
            students.setStudents(studentsList);
            File file = new File(fileName + ".xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Students.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(students, file);
        } catch (JAXBException e) {
            throw new AssignmentExceptions("Error while generating xml file !!!", e);
        }
    }

    public Map<Integer, Student> ReadFromXmlFile(String fileName) throws AssignmentExceptions {
        Map<Integer, Student> data = new LinkedHashMap<>();
        try {

            File file = new File(fileName + ".xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Students.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Students students = (Students) jaxbUnmarshaller.unmarshal(file);

            for (int i = 0; i < students.getStudents().size(); i++) {
                data.put(i + 1, students.getStudents().get(i));
            }
        } catch (JAXBException e) {
            throw new AssignmentExceptions("Error while reading from xml file !!!", e);
        }
        return data;
    }
}
