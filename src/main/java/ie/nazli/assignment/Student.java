package ie.nazli.assignment;

import java.util.Date;
import java.util.Map;

public class Student {
    private int id;
    private String firstName;
    private String surname;
    private String address;
    private Date birthDate;
    private String birthCity;

    public Student(int id, String firstName, String surname, String address, Date birthDate, String birthCity) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.address = address;
        this.birthDate = birthDate;
        this.birthCity = birthCity;
    }

    public Student(int id, String firstName, String surname) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
    }

    public String getFullName() {
        return this.firstName + " " + this.surname;
    }

    public void setId(int id) {

        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public Date getBirthDate() {
        return birthDate;
    }


    public Student() {
    }

    public String getBirthCity() {
        return birthCity;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", address='" + address + '\'' +
                ", birthDate=" + birthDate +
                ", birthCity='" + birthCity + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (id != student.id) return false;
        if (firstName != null ? !firstName.equals(student.firstName) : student.firstName != null) return false;
        if (surname != null ? !surname.equals(student.surname) : student.surname != null) return false;
        if (address != null ? !address.equals(student.address) : student.address != null) return false;
        if (birthDate != null ? !birthDate.equals(student.birthDate) : student.birthDate != null) return false;
        return birthCity != null ? birthCity.equals(student.birthCity) : student.birthCity == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (birthCity != null ? birthCity.hashCode() : 0);
        return result;
    }

    public Map<Integer, Student> addNewStudent(Student student, Map<Integer, Student> data) {
        if (studentExist(student, data) == -1) {
            data.put(student.getId(), student);
        } else {
            System.out.println("The student you want to add is already exist.");
        }
        return data;
    }

    public int studentExist(Student student, Map<Integer, Student> data) {
        int result = -1;
        for (Student s : data.values()) {
            if (s.getFullName().equals(student.getFullName())) {
                result = s.getId();
                break;
            }
        }
        return result;
    }

    public Map<Integer, Student> deleteStudent(String fullName, Map<Integer, Student> data) {
        String[] s = fullName.split(" ");
        Student student = new Builder().withId(1).withFirstName(s[0]).withSurname(s[1]).build();
        int id = studentExist(student, data);
        if (id != -1) {
            data.remove(id);
        } else {
            System.out.println("The student you want to delete is not exist!!!");
        }
        return data;
    }

    public Map<Integer, Student> updateStudent(String fullName, Student student, Map<Integer, Student> data) {
        String[] s = fullName.split(" ");
        Student oldStudent = new Builder().withId(1).withFirstName(s[0]).withSurname(s[1]).build();
        int id = studentExist(oldStudent, data);
        if (id != -1) {
            data.get(id).setFirstName(student.getFirstName());
            data.get(id).setSurname(student.getSurname());
            data.get(id).setAddress(student.getAddress());
            data.get(id).setBirthDate(student.getBirthDate());
            data.get(id).setBirthCity(student.getBirthCity());
        } else {
            System.out.println("The student you want to update is not exist!!!");
        }
        return data;
    }

    public static class Builder {
        private int id;
        private String firstName;
        private String surname;
        private String address;
        private Date birthDate;
        private String birthCity;

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder withAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder withBirthDate(Date birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Builder withBirthCity(String birthCity) {
            this.birthCity = birthCity;
            return this;
        }

        public Student build() {
            return new Student(id, firstName, surname, address, birthDate, birthCity);
        }
    }
}
