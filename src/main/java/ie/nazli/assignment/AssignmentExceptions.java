package ie.nazli.assignment;

public class AssignmentExceptions extends Exception {

    public AssignmentExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
