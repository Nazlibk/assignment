package ie.nazli.assignment;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "students")
public class Students {

    private List<Student> students = null;

    public List<Student> getStudents() {
        return students;
    }

    @XmlElement(name = "student")
    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
